var baseUrl = '/blog_assignment/admin/';

const auth = new Authentication(baseUrl);


function validateForm(thisForm) {
  'use strict';
  let res = true;
  // Loop over them and prevent submission
  var validation = Array.prototype.filter.call(thisForm, function (form) {

   if (form.checkValidity() === false) {
      res = false;
    }
    thisForm.classList.add('was-validated');

  });
  return res;
}

function submitSignInForm() {
	auth.thisForm = 'signin_frm';
  	auth.submitSignInForm();
 }


function submitUserForm(){
	auth.thisForm = 'user_frm';
	auth.submitUserForm();
}

function validatePw2(pw2) {
	auth.validatePw2(pw2);
}

function showModal(header,message){
	$('#myModal_head').html(header);
	$('#myModal_body').html(message);
	$('#myModal').modal('show');
}
