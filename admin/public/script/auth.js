class Authentication {
	_baseUrl;
	_thisForm;
	_passwordName;

	constructor(baseUrl) {
		this._baseUrl = baseUrl;
		this._passwordName = 'pwd';
	}

	get thisForm() {
		return this._thisForm;
	}
	set thisForm(x) {
		this._thisForm = x;
	}

	submitSignInForm() {
		var thisForm = document.getElementById(this._thisForm);
		let validateRes = validateForm(thisForm);

		if (validateRes) {
			this.createHashPassword();

			var action = $(`#${this._thisForm}`).attr('action');
			$.ajax({
				type: "POST",
				url: action, // the URL of the controller action method
				data: $(`#${this._thisForm}`).serialize(), // optional data
				success: function (result, para) {
					if (para == 'success') {
						if (result == 'login true') {
							window.location.href = `${baseUrl}home/index`;
						} else {
							showModal('Sign in', result)
						}
					}
				}
			});
		}
	}

	createHashPassword() {
		let thisForm = document.getElementById(this._thisForm);
		let pwd = document.getElementById(this._passwordName);
		let rep_pwd = document.getElementById(this._passwordRepName);

		// Create a new element input, this will be our hashed password field. 
		var p = document.createElement("input");

		// Add the new element to our form. 
		thisForm.appendChild(p);
		p.name = "p";
		p.type = "hidden";
		p.value = hex_sha512(pwd.value);

		// Make sure the plaintext password doesn't get sent. 
		pwd.value = '';
		if (rep_pwd) rep_pwd.value = '';
	}
	
	submitUserForm() {
		let thisForm = document.getElementById(this._thisForm);
		let validateRes = validateForm(thisForm);

		if (validateRes) {
			this.createHashPassword();

			let action = $(`#${this._thisForm}`).attr('action');
			$.ajax({
				type: "POST",
				url: action, // the URL of the controller action method
				data: $(`#${this._thisForm}`).serialize(), // optional data
				success: (result, para) => {
					if (para == 'success') {
						var result = JSON.parse(result);
						var msg = result['msg'];

						showModal('Add/Edit User', msg)

						resetForm(this._thisForm)
					}
				}
			});
		}
	}

	validatePw2(pw2) {
		if (pw2.value !== $("#pwd").val()) {
			pw2.setCustomValidity("Duplicate passwords do not match");
			$('#rep_pwd_invalid_feedback').html('Duplicate passwords do not match');
		} else {
			pw2.setCustomValidity(""); // is valid
		}
	}

}