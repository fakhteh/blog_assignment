<?php
class Database{
	
private $host = "";  
private $user = "";  
private $pass = "";  
private $dbname = "";

private $stmt; 	

private $dbh;  
private $error;
private $results;	
public static $DBConnection;	
//************************************************************************
//************************************************************************	
private function __CONSTRUCT(){  
	$this->host = "127.0.0.1";
	$this->user = "root";
	$this->pass = "";
	$this->dbname = "blog-post-db";
	
	// Set DSN  
	$dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;  
	// Set options  
	$options = array(  
	PDO::ATTR_PERSISTENT => true,  
	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION  
	);  
	// Create a new PDO instanace  
	try{  
		$this->dbh = new PDO($dsn, $this->user, $this->pass, $options);  
	}  
	// Catch any errors  
	catch(PDOException $e){  
		$this->error = $e->getMessage();  
	}  
} 
//************************************************************************
//************************************************************************
public static function getConnection(){
	
	if(empty(Database::$DBConnection)){
		Database::$DBConnection = new Database();
	}
	return Database::$DBConnection;
}
	
//************************************************************************
//************************************************************************	
public function query($query){  
	$this->stmt = $this->dbh->prepare($query);  
}  	
//************************************************************************
//************************************************************************
public function bind($param, $value, $type = null){  
	if (is_null($type)) {  
		switch (true) {  
			case is_int($value):  
				$type = PDO::PARAM_INT;  
				break;  
			case is_bool($value):  
				$type = PDO::PARAM_BOOL;  
				break;  
			case is_null($value):  
				$type = PDO::PARAM_NULL;  
				break;  
			default:  
				$type = PDO::PARAM_STR;  
		}  
	}  
	$this->stmt->bindValue($param, $value, $type);  
}  	
//************************************************************************
//************************************************************************	
public function execute(){  
	return $this->stmt->execute();  
}  
//************************************************************************
//************************************************************************	
/*
The Result Set function returns an array of the result set rows.
First we run the execute method, then we return the results.
*/
public function resultset(){
	$this->execute();  
	/*-- was wrote from other class by fakhteh to save results ----------------*/
	// You can PDO::FETCH_OBJ instad of assoc, or whatever you like
	//$this->results = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	/*-------------------------------------------------------------------------*/
	return $this->stmt->fetchAll(PDO::FETCH_ASSOC);  
}  	
//************************************************************************
//************************************************************************	
/*
Very similar to the previous method, the Single method simply returns a single record from the database. 
*/
public function single(){  
	$this->execute();  
	return $this->stmt->fetch(PDO::FETCH_ASSOC);  
}  
//************************************************************************
//************************************************************************	
/*
returns the number of effected rows from the previous delete, update or insert statement.
*/
public function rowCount(){  
	return $this->stmt->rowCount();  
}
//************************************************************************
//************************************************************************	
/* returns the last inserted Id as a string.  */	
public function lastInsertId(){  
	return $this->dbh->lastInsertId();  
} 	
//************************************************************************
//************************************************************************	
/*
Transactions allows you to run multiple changes to a database all in one batch to ensure that your work will not be accessed incorrectly or there will be no outside interferences before you are finished. If you are running many queries that all rely upon each other, if one fails an exception will be thrown and you can roll back any previous changes to the start of the transaction.
*/
public function beginTransaction(){  
	return $this->dbh->beginTransaction();  
} 	
//************************************************************************
//************************************************************************	
/*	To end a transaction and commit your changes */
public function endTransaction(){  
	return $this->dbh->commit();  
} 	
//************************************************************************
//************************************************************************	
/* To cancel a transaction and roll back your changes */
public function cancelTransaction(){  
	return $this->dbh->rollBack();  
} 	
//************************************************************************
//************************************************************************
public function debugDumpParams(){  
	return $this->stmt->debugDumpParams();  
}	
//************************************************************************
//************************************************************************	
/*
 * private function log_error( string $msg )
 *      @string $msg - The message to write into the log file
 * 
 * Writes errors in a log file.
 */


//************************************************************************
//************************************************************************	
/*private function  __Error_Handeler($Exception)
 {
	 //throw  new \Exception($Application_Error_List["$ErrorCode"]['Covered_Message'] , $Application_Error_List["$ErrorCode"]['Code']);
	 //throw new MyDatabaseException( $Exception->getMessage( ) , (int)$Exception->getCode( ) );
 }	*/
//************************************************************************
//************************************************************************	
/*public static function getInstance() {
	if (is_null(self::$instance)) {
		self::$instance = new Database();
	}
	return self::$instance;
}*/	
//************************************************************************
//************************************************************************	
public static function closeConnection(){
//	$this->dbh = null;
//	$this->stmt = null;
	Database::$DBConnection = null;
}
//************************************************************************
//************************************************************************		
public function results() {
	return $this->results;
}
public function first() {
	return $this->results[0];
}
public function last() {
	return $this->results[$this->count-1];
}
public function row($id) {
	return $this->results[$id];
}		
//************************************************************************
//************************************************************************		
}  

//$db = new Database();
/* how to use it
Using your PDO class
Now that we’ve finished writing the database class, it’s time to test it out.

The remaining bit of this tutorial will be using the tutorial.php file you created at the very start.

I’m going to be using the same MySQL table that I created in last week’s tutorial. If you haven’t read last week’s tutorial, you can create the table by running the following SQL.
[sql]
CREATE TABLE mytable (
ID int(11) NOT NULL AUTO_INCREMENT,
FName varchar(50) NOT NULL,
LName varchar(50) NOT NULL,
Age int(11) NOT NULL,
Gender enum(‘male’,’female’) NOT NULL,
PRIMARY KEY (ID)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;
[/sql]

Insert a new record
Firstly you need to instantiate a new database.

$database = new Database();  
Next we need to write our insert query. Notice how I’m using placeholders instead of the actual data parameters.

$database->query(‘INSERT INTO mytable (FName, LName, Age, Gender) VALUES (:fname, :lname, :age, :gender)’);  
Next we need to bind the data to the placeholders.

$database->bind(‘:fname’, ‘John’);  
$database->bind(‘:lname’, ‘Smith’);  
$database->bind(‘:age’, ’24’);  
$database->bind(‘:gender’, ‘male’);  
And finally we run execute the statement.

$database->execute();  
Before running the file, echo out the lastInsertId function so you will know that the query successfully ran when viewed in the browser.

echo $database->lastInsertId();  
Insert multiple records using a Transaction
The next test we will try is to insert multiple records using a Transaction so that we don’t have to repeat the query.

The first thing we need to do is to begin the Transaction.

$database->beginTransaction();  
Next we set the query.

$database->query(‘INSERT INTO mytable (FName, LName, Age, Gender) VALUES (:fname, :lname, :age, :gender)’);  
Next we bind the data to the placeholders.

$database->bind(‘:fname’, ‘Jenny’);  
$database->bind(‘:lname’, ‘Smith’);  
$database->bind(‘:age’, ’23’);  
$database->bind(‘:gender’, ‘female’);  
And then we execute the statement.

$database->execute();  
Next we bind the second set of data.

$database->bind(‘:fname’, ‘Jilly’);  
$database->bind(‘:lname’, ‘Smith’);  
$database->bind(‘:age’, ’25’);  
$database->bind(‘:gender’, ‘female’);  
And run the execute method again.

$database->execute();  
Next we echo out the lastInsertId again.

echo $database->lastInsertId();  
And finally we end the transaction

$database->endTransaction();  
Select a single row
The next thing we will do is to select a single record.

So first we set the query.

$database->query(‘SELECT FName, LName, Age, Gender FROM mytable WHERE FName = :fname’);  
Next we bind the data to the placeholder.

$database->bind(‘:fname’, ‘Jenny’);  
Next we run the single method and save it into the variable $row.

$row = $database->single();  
Finally, we print the returned record to the screen.

echo "<pre>";  
print_r($row);  
echo "</pre>";  
Select multiple rows
The final thing we will do is to run a query and return multiple rows.

So once again, set the query.

$database->query(‘SELECT FName, LName, Age, Gender FROM mytable WHERE LName = :lname’);  
Bind the data.

$database->bind(‘:lname’, ‘Smith’);  
Run the resultSet method and save it into the $rows variable.

$rows = $database->resultset();  
Print the return records to the screen.

echo "<pre>";  
print_r($rows);  
echo "</pre>";  
And finally display the number of records returned.

echo $database->rowCount(); 
*/
?>