<?php
require(CONFIG.'Error_Log.php');
use log_errors\Error_Log;
//****************************************************************
//****************************************************************
//************************                  **********************
//*******************     	bridge Class       *******************
//************************                  **********************
//****************************************************************
//****************************************************************

class bridge
{
private  $id = 0;
private  $user_id = "";	
private  $post_id = "";
	
private  $db;
private  $table_name = "bridge";
private  $Query_Result;
//------------------------------------------------------------------------------------------------------
public function __construct($db)
{
	$this->db=$db;
}
//------------------------------------------------------------------------------------------------------
private function createTable()
{
	try {
		$sql="CREATE TABLE bridge
				  (
				  id       			mediumint UNSIGNED AUTO_INCREMENT primary key ,
				  user_id          	mediumint UNSIGNED ,
				  post_id			mediumint UNSIGNED 
				  );";
		$this->db->query($sql);
		return $this->db->execute();
	}catch (Exception $e)
	{
		return false; 
		 $this->__Error_Handeler($e->getMessage()); 
		 $this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
   }
}
//------------------------------------------------------------------------------------------------------	
public function getId()
{
		return $this->id;
}
//------------------------------------------------------------------------------------------------------
public function SetItems($id,$post_id,$user_id)
{
	$this->id = $id;
	$this->post_id = $title;
	$this->user_id = $user_id;
}
//------------------------------------------------------------------------------------------------------
public function GetItems()
{
	$items['id']  		 	= $this->id;
	$items['post_id']  	 	= $this->post_id;
	$items['user_id']   	= $this->user_id;
	
	return $items;
}
//------------------------------------------------------------------------------------------------------
public function AddItem()
{
	try {
		$this->db->query('INSERT INTO '.$this->table_name.' ( 
				  post_id
				 ,user_id
				) 
				values 
				(	         
				   :post_id
				  ,:user_id
				) '
		); 

		$this->db->bind(':post_id',		$this->post_id);
		$this->db->bind(':user_id',		$this->user_id);
		
		$add_res = $this->db->execute();
		$this->id = $this->db->lastInsertId();
		return $add_res;
	}catch (\Exception $e)
	{
		$this->__Error_Handeler($e->getMessage()); 
		$this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
		
        return false; 
	}
	
}
//------------------------------------------------------------------------------------------------------
private function logError($User, $Err, $Location,$method)
{
	new Error_Log(ROOT);
	Error_Log::DP_SetError($User, $Err, $Location,$method);

}	
//--------------------------------------------------------------------------------------------------------
private function  __Error_Handeler($Exception)
 {
	echo "Exception=>" .$Exception;
 }	
//-------------------------------------------------------------------------------------------------------	
}

?>
