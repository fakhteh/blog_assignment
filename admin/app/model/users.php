<?php
require(CONFIG.'Error_Log.php');
use log_errors\Error_Log;
//****************************************************************
//****************************************************************
//************************                  **********************
//*******************     	users Class       ********************
//************************                  **********************
//****************************************************************
//****************************************************************

class users
{
private  $id = 0;
private  $name = "";	
private  $user_name = "";
private  $password = "";
private  $user_type = "";	

private  $db;
private  $table_name = "users";
private  $Query_Result;
//------------------------------------------------------------------------------------------------------
public function __construct($db)
{
	$this->db=$db;
}
//------------------------------------------------------------------------------------------------------
private function createTable()
{
	try {
		$sql="CREATE TABLE users
				  (
				  id       			mediumint UNSIGNED AUTO_INCREMENT primary key ,
				  name          	varchar(250) ,
				  user_name			varchar(250) ,
				  password			varchar(250) ,
				  user_type			enum('a', 'u') DEFAULT 'u',
				  
				  INDEX (user_name)
				  );";
		$this->db->query($sql);
		return $this->db->execute();
	}catch (Exception $e)
	{
		return false; 
		 $this->__Error_Handeler($e->getMessage()); 
		 $this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
   }
}
//------------------------------------------------------------------------------------------------------	
public function getId()
{
		return $this->id;
}
//------------------------------------------------------------------------------------------------------
public function SetItems($id,$name,$user_name,$password,$user_type)
{
	$this->id = $id;
	$this->name = $name;
	$this->user_name = $user_name;
	$this->password = $password;
	$this->user_type = $user_type;
}
//------------------------------------------------------------------------------------------------------
public function GetItems()
{
	$items['id']  		 = $this->id;
	$items['name']  	 = $this->name;
	$items['user_name']  = $this->user_name;
	$items['password']   = $this->password;
	$items['user_type']  = $this->user_type;
	
	return $items;
}
//------------------------------------------------------------------------------------------------------
public function SelectAll(){
	try{
		$this->db->query('SELECT * FROM '.$this->table_name.' ORDER BY id DESC');
		$rows = $this->db->resultset();
		return $rows;

	}catch (\Exception $e)
  	{ 
	  $this->__Error_Handeler("2001", $e); 
	  return false; 
  	} 
}
public function SearchItemByUsername($username)
{
	try{
		$this->db->query('SELECT id,name,user_name,password FROM '.$this->table_name.' WHERE user_name = :user_name');
		$this->db->bind(':user_name', $username );
		$rows = $this->db->resultset();
		
		$this->id = $rows[0]['id'];
		$this->name = $rows[0]['name'];
		$this->user_name = $rows[0]['user_name'];
		$this->user_type = $rows[0]['user_type'];
		$this->password = $rows[0]['password'];
		
		return true;

	}catch (\Exception $e)
  	{ 
	  	$this->__Error_Handeler($e->getMessage()); 
		 $this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
         return false;  
  	} 
}	
//------------------------------------------------------------------------------------------------------
public function AddItem()
{
	try {
		$this->db->query('INSERT INTO '.$this->table_name.' ( 
				  name
				 ,user_name
				 ,password
				 ,user_type
				) 
				values 
				(	         
				   :name
				  ,:user_name
				  ,:password
				  ,:user_type
				) '
		); 

		$this->db->bind(':name',		$this->name);
		$this->db->bind(':user_name',	$this->user_name);
		$this->db->bind(':password',	$this->password);
		$this->db->bind(':user_type',	$this->user_type);
		
		$add_res = $this->db->execute();
		$this->id = $this->db->lastInsertId();
		return $add_res;
	}catch (\Exception $e)
	{
		$this->__Error_Handeler($e->getMessage()); 
		$this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
		
        return false; 
	}
	
}
//------------------------------------------------------------------------------------------------------
public function SearchItem($id)
{
	try{
		$this->db->query('SELECT * FROM '.$this->table_name.' WHERE id = :id');
		$this->db->bind(':id', $id );
		$rows = $this->db->resultset();
		
		$this->id 		  = $rows[0]['id'];
		$this->name  	  =	$rows[0]['name'];
		$this->user_name  =	$rows[0]['user_name'];
		$this->password   =	$rows[0]['password'];
		$this->user_type  =	$rows[0]['user_type'];
		
		return true;

	}catch (\Exception $e)
  	{ 
	  	$this->__Error_Handeler($e->getMessage()); 
		 $this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
         return false;  
  	} 
}
	
//------------------------------------------------------------------------------------------------------
private function logError($User, $Err, $Location,$method)
{
	new Error_Log(ROOT);
	Error_Log::DP_SetError($User, $Err, $Location,$method);

}	
//--------------------------------------------------------------------------------------------------------
private function  __Error_Handeler($Exception)
 {
	echo "Exception=>" .$Exception;
 }	
//-------------------------------------------------------------------------------------------------------	
}

?>
