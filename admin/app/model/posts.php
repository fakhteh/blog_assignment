<?php
require(CONFIG.'Error_Log.php');
use log_errors\Error_Log;
//****************************************************************
//****************************************************************
//************************                  **********************
//*******************     	posts Class       ********************
//************************                  **********************
//****************************************************************
//****************************************************************

class posts
{
private  $id = 0;
private  $title = "";	
private  $description = "";
private  $user_id = "";
private  $created_at = "";	

private  $db;
private  $table_name = "posts";
private  $Query_Result;
//------------------------------------------------------------------------------------------------------
public function __construct($db)
{
	$this->db=$db;
}
//------------------------------------------------------------------------------------------------------
private function createTable()
{
	try {
		$sql="CREATE TABLE posts
				  (
				  id       			mediumint UNSIGNED AUTO_INCREMENT primary key ,
				  title          	varchar(250) ,
				  description		varchar(250) ,
				  user_id			mediumint UNSIGNED NOT NULL ,
				  created_at		timestamp DEFAULT CURRENT_TIMESTAMP				  
				  );";
		$this->db->query($sql);
		return $this->db->execute();
	}catch (Exception $e)
	{
		return false; 
		 $this->__Error_Handeler($e->getMessage()); 
		 $this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
   }
}
//------------------------------------------------------------------------------------------------------	
public function getId()
{
		return $this->id;
}
//------------------------------------------------------------------------------------------------------
public function SetItems($id,$title,$description,$user_id,$created_at)
{
	$this->id = $id;
	$this->title = $title;
	$this->description = $description;
	$this->user_id = $user_id;
	$this->created_at = $created_at;
}
//------------------------------------------------------------------------------------------------------
public function GetItems()
{
	$items['id']  		 	= $this->id;
	$items['title']  	 	= $this->title;
	$items['description']  	= $this->description;
	$items['user_id']   	= $this->user_id;
	$items['created_at']  	= $this->created_at;
	
	return $items;
}
//------------------------------------------------------------------------------------------------------
public function SelectAll(){
	try{
		$this->db->query('SELECT * FROM '.$this->table_name.' ORDER BY id DESC');
		$rows = $this->db->resultset();
		return $rows;

	}catch (\Exception $e)
  	{ 
	  $this->__Error_Handeler("2001", $e); 
	  return false; 
  	} 
}
public function SearchItemByUserId($user_id)
{
	try{
		$this->db->query('SELECT * FROM '.$this->table_name.' WHERE user_id = :user_id');
		$this->db->bind(':user_id', $user_id );
		$rows = $this->db->resultset();
		
		$this->id 			= $rows[0]['id'];
		$this->title 		= $rows[0]['title'];
		$this->description 	= $rows[0]['description'];
		$this->user_id 		= $rows[0]['user_id'];
		$this->created_at 	= $rows[0]['created_at'];
		
		return true;

	}catch (\Exception $e)
  	{ 
	  	$this->__Error_Handeler($e->getMessage()); 
		 $this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
         return false;  
  	} 
}	
//------------------------------------------------------------------------------------------------------
public function AddItem()
{
	try {
		$this->db->query('INSERT INTO '.$this->table_name.' ( 
				  title
				 ,description
				 ,user_id
				 ,created_at
				) 
				values 
				(	         
				   :title
				  ,:description
				  ,:user_id
				  ,:created_at
				) '
		); 

		$this->db->bind(':title',		$this->title);
		$this->db->bind(':description',	$this->description);
		$this->db->bind(':user_id',		$this->user_id);
		$this->db->bind(':created_at',	$this->created_at);
		
		$add_res = $this->db->execute();
		$this->id = $this->db->lastInsertId();
		return $add_res;
	}catch (\Exception $e)
	{
		$this->__Error_Handeler($e->getMessage()); 
		$this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
		
        return false; 
	}
	
}
//------------------------------------------------------------------------------------------------------
public function SearchItem($id)
{
	try{
		$this->db->query('SELECT * FROM '.$this->table_name.' WHERE id = :id');
		$this->db->bind(':id', $id );
		$rows = $this->db->resultset();
		
		$this->id 		  	= $rows[0]['id'];
		$this->title  	  	=	$rows[0]['title'];
		$this->description  =	$rows[0]['description'];
		$this->user_id   	=	$rows[0]['user_id'];
		$this->created_at  	=	$rows[0]['created_at'];
		
		return true;

	}catch (\Exception $e)
  	{ 
	  	$this->__Error_Handeler($e->getMessage()); 
		 $this->logError('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
         return false;  
  	} 
}
	
//------------------------------------------------------------------------------------------------------
private function logError($User, $Err, $Location,$method)
{
	new Error_Log(ROOT);
	Error_Log::DP_SetError($User, $Err, $Location,$method);

}	
//--------------------------------------------------------------------------------------------------------
private function  __Error_Handeler($Exception)
 {
	echo "Exception=>" .$Exception;
 }	
//-------------------------------------------------------------------------------------------------------	
}

?>
