<?php
require(CONFIG.'Sessions.php');
require(CONFIG.'Constants.php');
class authController extends Controller {
	
	public function signupForm(){
		$this->view('auth/signup',[]);
		$this->view->set_page_title("Sign Up");
		$this->view->render();
	}
	
	public function signinForm(){
		$this->view('auth/signin',[]);
		$this->view->set_page_title("Sign In");
		$this->view->render();
	}
	
	public function signin(){
		
		$uname 		= htmlspecialchars($_POST['uname']);
		$p 			= htmlspecialchars($_POST['p']);
		$error_msg 	= '';
		
		if(isset($uname) && isset($p)){
			if($uname == 'Admin' && $p == "ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413"){
				Sessions::set_sessions($uname);
			}else{
				$error_msg .= 'Error! please try again.';
			}
		}else{
			$error_msg .= 'Error! please try again.';
		}
		
		if(empty($error_msg)){
			echo "login true";
		}else{
			echo $error_msg;
		}
	}
	
	
}
