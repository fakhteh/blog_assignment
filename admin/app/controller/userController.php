<?php
require(CONFIG.'Constants.php');

class userController extends Controller {
	
	public function index(){
		
		$this->model('users');
		$users = $this->model->SelectAll();
		
		$users_cnt = count($users);
		
		$this->view('user/index',[ "users" => $users ]);
		
		$this->view->set_page_title("Users");
		$this->view->render();
	}
	
	public function addEdit(){
		$name 		= htmlspecialchars($_POST['name']);
		$uname 		= htmlspecialchars($_POST['uname']);
		$user_type 	= htmlspecialchars($_POST['user_type']);
		$p 			= htmlspecialchars($_POST['p']);
		
		/////////////////////////////////////////////////////
		$add_res = false;
		$error_msg = '';
		if (isset($name, $uname, $user_type, $p)) {
			$this->model('users');
			
				$name  = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
				$uname = filter_input(INPUT_POST, 'uname', FILTER_SANITIZE_EMAIL);
    			$uname = filter_var($uname, FILTER_VALIDATE_EMAIL);
				if (!$uname) {
					// Not a valid email
					$error_msg .= 'The email address you entered is not valid';
				}
				
				$password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
				if (strlen($password) != 128) {
					// The hashed pwd should be 128 characters long.
					// If it's not, something really odd has happened
					$error_msg .= 'Invalid password configuration';
				}
				
				if($uname>0){
					$res_search_username = $this->model->SearchItemByUsername($uname);
					$username_rowCount = $this->db->rowCount();
				}else{
					$username_rowCount = 0;
				}
				
				if($username_rowCount){
					// A user with this email address already exists
					$error_msg .= 'A user with this email address already exists';
				}
				if (empty($error_msg)) {
					//Insert the new user into the "users"
					$this->model->SetItems('',$name,$uname,$p,$user_type);
					$res = $this->model->AddItem();
					$add_res = $res;
					
				}else{
					$add_res = false;
				}
		}
		$message = ($add_res)? Constants::$save_success_msg:Constants::$save_error_msg;
		$data = array("msg" => $message);
		echo json_encode($data);
		/////////////////////////////////////////////////////
	}
}
